<aside id="sidebar_left" class="nano affix">
            <!-- Sidebar Left Wrapper  -->
            <div class="sidebar-left-content nano-content">
                <!-- Sidebar Header -->
                <header class="sidebar-header">
                    <!-- Sidebar - Logo -->
                    <div class="sidebar-widget logo-widget">
                        <div class="media">
                            <a class="" href="accueil.php">
                                <img src="http://africawebfestival.com/wp-content/uploads/2019/05/Logoawf2019.png" alt="" class="img-responsive" width="250" style="margin: 10px;">
                            </a>
                        </div>
                    </div>
                </header>
                <!-- /Sidebar Header -->
                <!-- Sidebar Menu  -->
                <ul class="nav sidebar-menu">
                    <li class="sidebar-label pt30">Navigation</li>
                    <li>
                        <a class="accordion-toggle menu-open" href="#">
                            <span class="caret"></span>
                            <span class="sidebar-title">Tableau de bord</span>
                            <span class="sb-menu-icon fa fa-home"></span>
                        </a>
                        <ul class="nav sub-nav">
                            <li class="active">
                                <a href="accueil.php">
                                Page d'accueil
                            </a>
                            </li>
                        </ul>
                    </li>
                    <li class="sidebar-label pt25">Services</li>
                    <li>
                        <a class="accordion-toggle " href="#">
                            <span class="caret"></span>
                            <span class="sidebar-title">AWF Actu</span>
                            <span class="sb-menu-icon fa fa-list-ul"></span>
                        </a>
                        <ul class="nav sub-nav">
                            <li class="">
                                <a href="user-forms-editors.php">
                                Ajouter un nouvel article
                            </a>
                            </li>
                            <li class="">
                                <a href="list_article.php">
                                Consulter les articles
                            </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a class="accordion-toggle " href="#">
                            <span class="caret"></span>
                            <span class="sidebar-title">Speakers AWF</span>
                            <span class="sb-menu-icon fa fa-list-ul"></span>
                        </a>
                        <ul class="nav sub-nav">
                            <li class="">
                                <a href="add-speaker.php">
                                Ajouter un speaker
                            </a>
                            </li>
                            <li class="">
                                <a href="list_speaker.php">
                                Consulter les speakers
                            </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a class="accordion-toggle " href="#">
                            <span class="caret"></span>
                            <span class="sidebar-title">Thématiques AWF</span>
                            <span class="sb-menu-icon fa fa-list-ul"></span>
                        </a>
                        <ul class="nav sub-nav">
                            <li class="">
                                <a href="add-thematiques.php">
                                Ajouter une thématiques
                            </a>
                            </li>
                            <li class="">
                                <a href="list_thematiques.php">
                                Consulter les thématiques
                            </a>
                            </li>
                        </ul>
                    </li>
                </ul>
                <!-- /Sidebar Menu  -->
            </div>
            <!-- /Sidebar Left Wrapper  -->
        </aside>
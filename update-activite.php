<?php
session_start();
require './database.php';

if (!empty($_GET['id'])) 
    {  $id = inputVerify($_GET['id']);

        $db = Database::connect();
        $statement = $db->query('SELECT * FROM activites where id ='.$id);
        $item = $statement->fetch();
    
    }

//creation de variable
$heure = $titre = $description = $jour = "";

//fonction de contrôle de champs (faille xss)
function inputVerify($var){

    $var = trim($var);
    $var = htmlspecialchars($var);
    $var = stripslashes($var);

    return $var;
}

if (!empty($_POST)) {
    $heure = inputVerify($_POST["heure"]);
    $titre = inputVerify($_POST["titre"]);
    $jour = inputVerify($_POST["jour"]);
    $description = inputVerify($_POST["description"]);

    $isUploadSuccess = true;        
    
    if($isUploadSuccess){
        $db = Database::connect();
            $statement = $db->prepare("UPDATE activites set heure = ?,title = ?,description = ?, id_jour = ? WHERE id=?");
            $statement->execute(array($heure, $titre, $description, $jour, $id));
            Database::disconnect();
            header("Location: ./list_activites.php");
    }
    
}
?>

<!DOCTYPE html>
<html>
<head>
    <!-- Meta and Title -->
    <meta charset="utf-8">
    <title>AWFAPP - modifier le programme du jeudi</title>
    <meta name="keywords" content="HTML5, Bootstrap 3, Admin Template, UI Theme" />
    <meta name="description" content="AWFAPP - L'application administrative de gestion de l'application AWFAPP">
    <meta name="author" content="ThemeREX">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Favicon -->
    <link rel="shortcut icon" href="assets/img/favicon.png">
    <!-- Angular material -->
    <link rel="stylesheet" type="text/css" href="assets/skin/css/angular-material.min.css">
    <!-- Icomoon -->
    <link rel="stylesheet" type="text/css" href="assets/fonts/icomoon/icomoon.css">
    <!-- AnimatedSVGIcons -->
    <link rel="stylesheet" type="text/css" href="assets/fonts/animatedsvgicons/css/codropsicons.css">
    <!-- Magnific popup -->
    <link rel="stylesheet" type="text/css" href="assets/js/plugins/magnific/magnific-popup.css">
    <!-- c3charts -->
    <link rel="stylesheet" type="text/css" href="assets/js/plugins/c3charts/c3.min.css">
    <!-- CSS - allcp forms -->
    <link rel="stylesheet" type="text/css" href="assets/allcp/forms/css/forms.css">
    <!-- mCustomScrollbar -->
    <link rel="stylesheet" type="text/css" href="assets/js/utility/malihu-custom-scrollbar-plugin-master/jquery.mCustomScrollbar.min.css">
    <!-- CSS - theme -->
    <link rel="stylesheet" type="text/css" href="assets/skin/default_skin/less/theme.css">
</head>
<body class="dashboard-page with-customizer">
    <!-- Body Wrap  -->
    <div id="main">
        <!-- Header  -->
        <?php include "./partials/top-header.php"; ?>
        <!-- /Header -->
        <!-- Sidebar  -->
        <?php include "./partials/side-header.php"; ?>
        <!-- /Sidebar -->
        <!-- Main Wrapper -->
        <section id="content_wrapper">
            <!-- Topbar -->
            <header id="topbar" class="alt">
                <div class="topbar-left">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-link">
                            <a href="accueil.php">Accueil</a>
                        </li>
                        <li class="breadcrumb-current-item">Tableau de bord</li>
                    </ol>
                </div>
            </header>
            <!-- /Topbar -->
            <!-- Content -->
            <section id="content" class="container col-lg-12">
                    <form action="ajouter_jour.php" method="post" class="col-lg-12" style="margin-bottom:15px;">
                        <h3>Modifier une activité a la journée</h3>

                        <label for="heure">Ajouter une heure
                            <input type="text" class="form-control" name="heure" id="heure" value="<?php echo $item['heure']; ?>">
                        </label>
                        <br>
                        <label for="title">Ajouter un titre
                            <input type="text" class="form-control" name="titre" id="title" value="<?php echo $item['title']; ?>">
                        </label>
                        <br>
                        <label for="">Choisir le jour
                            <select name="jour" id="" class="form-control">
                                <option value="1">Mercredi</option>
                                <option value="2">Jeudi</option>
                                <option value="3">Vendredi</option>
                                <option value="4">Samedi</option>
                            </select>
                        </label>
                        <br>
                        <label for="description">Ajouter une description
                            <textarea name="description" id="mytextarea">
                                <?php echo $item['description']; ?>
                            </textarea>
                        </label>
                        <br>
                        <input type="submit" class="btn btn-primary" value="Modifier">
                    </form>

            </section>
            <!-- /Content -->

            <!-- Header -->
            <?php include "./partials/footer.php"; ?>
            <!-- /Header -->
        </section>
        <!-- /Main Wrapper -->
    </div>
    <!-- /Body Wrap  -->
    <!-- Scripts -->
    <!-- jQuery -->
    <script src="assets/js/jquery/jquery-1.12.3.min.js"></script>
    <script src="assets/js/jquery/jquery_ui/jquery-ui.min.js"></script>
    <!-- AnimatedSVGIcons -->
    <script src="assets/fonts/animatedsvgicons/js/snap.svg-min.js"></script>
    <script src="assets/fonts/animatedsvgicons/js/svgicons-config.js"></script>
    <script src="assets/fonts/animatedsvgicons/js/svgicons.js"></script>
    <script src="assets/fonts/animatedsvgicons/js/svgicons-init.js"></script>
    <!-- Scroll -->
    <script src="assets/js/utility/malihu-custom-scrollbar-plugin-master/jquery.mCustomScrollbar.concat.min.js"></script>
    <!-- HighCharts Plugin -->
    <script src="assets/js/plugins/highcharts/highcharts.js"></script>
    <!-- Magnific Popup Plugin -->
    <script src="assets/js/plugins/magnific/jquery.magnific-popup.js"></script>
    <!-- Plugins -->
    <script src="assets/js/plugins/c3charts/d3.min.js"></script>
    <script src="assets/js/plugins/c3charts/c3.min.js"></script>
    <script src="assets/js/plugins/circles/circles.js"></script>
    <!-- Jvectormap JS -->
    <script src="assets/js/plugins/jvectormap/jquery.jvectormap.min.js"></script>
    <script src="assets/js/plugins/jvectormap/assets/jquery-jvectormap-us-lcc-en.js"></script>
    <script src="assets/js/plugins/jvectormap/assets/jquery-jvectormap-world-mill-en.js"></script>
    <!-- Theme Scripts -->
    <script src="assets/js/utility/utility.js"></script>
    <script src="assets/js/demo/demo.js"></script>
    <script src="assets/js/main.js"></script>
    <script src="assets/js/demo/widgets_sidebar.js"></script>
    <script src="assets/js/pages/dashboard1.js"></script>
    <script src="assets/js/demo/widgets.js"></script>
    <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>
  tinymce.init({
    selector: '#mytextarea'
  });
  </script>
    <!-- /Scripts -->
</body>
</html>
